package ca.qc.claurendeau.exercice_08;

// Simplification de if-else enchaînés

public class Account {
	boolean isDead;
	boolean isSeparated;
	boolean isRetired;

	public double getPayAmount() {
		double result;
		if (isDead){
			result = deadAmount();
		}
		else if (isSeparated){
			result = separatedAmount();
		}
		else if (isRetired){
			result = retiredAmount();
		}
		else {
			result = normalPayAmount();
		}	
		return result;
	}

	private double normalPayAmount() {
		// Ici pour remplacer la vrai méthode
		return 0;
	}

	private double retiredAmount() {
		// Ici pour remplacer la vrai méthode
		return 0;
	}

	private double separatedAmount() {
		// Ici pour remplacer la vrai méthode
		return 0;
	}

	private double deadAmount() {
		// Ici pour remplacer la vrai méthode
		return 0;
	}
}
